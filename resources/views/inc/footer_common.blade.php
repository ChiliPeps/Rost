{!! Html::script('js/jquery.js') !!}
{!! Html::script('js/bootstrap.min.js') !!}
{!! Html::script('js/classie.js') !!}
{!! Html::script('js/cbpAnimatedHeader.js') !!}
{!! Html::script('js/jqBootstrapValidation.js') !!}
{!! Html::script('js/contact_me.js') !!}
{!! Html::script('js/freelancer.js') !!}

@include('js.google_map')

{{-- carousel --}}
{{-- {!! Html::script('js/jquery-2.1.4.min.js') !!}
{!! Html::script('js/kenburns-text.js') !!}
{!! Html::script('js/sweetalert.min.js') !!} --}}