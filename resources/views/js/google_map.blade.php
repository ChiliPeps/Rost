
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?language=en"></script>
<script>
var map, map2;
var infoWindow;

// markersData variable stores the information necessary to each marker
var markersData = [
   {
      lat: 24.1300458,
      lng: -110.3142512,
      name: "Rosticeria California Colima",
      address1:"",
      address2: "",
      postalCode: ""
     // don't insert comma in the last item of each marker
   },
   {
      lat: 24.0673658,
      lng: -110.3060812,
      name: "Rosticeria California La Fuente",
      address1:"Quinta dos Patos, n.º 2",
      address2: "Praia da Costa Nova",
      postalCode: ""
       // don't insert comma in the last item of each marker
   },
   {
      lat: 24.1452802,
      lng: -110.3160805,
      name: "Rosticeria California Abasolo",
      address1:"Rua dos Balneários do Complexo Desportivo",
      address2: "Gafanha da Nazaré",
      postalCode: ""
       // don't insert comma in the last item of each marker
   } // don't insert comma in the last item
];


function initialize() {
   var mapOptions = {
      center: new google.maps.LatLng(24.1300458,-110.3142512),
      zoom: 0,
      scrollwheel: false,
   };

     var mapOptions2 = {
      center: new google.maps.LatLng(22.9018778,-109.9317387),
      zoom: 14,
      scrollwheel: false,
      mapMaker: new google.maps.LatLng(22.9018778,-109.9317387),
   };

   map = new google.maps.Map(document.getElementById('mapa1'), mapOptions);

   map2 = new google.maps.Map(document.getElementById('mapa2'), mapOptions2);

   
          

   // a new Info Window is created
   infoWindow = new google.maps.InfoWindow();

   // Event that closes the Info Window with a click on the map
   google.maps.event.addListener(map, 'click', function() {
      infoWindow.close();
   });

   // Finally displayMarkers() function is called to begin the markers creation
   displayMarkers();
}
google.maps.event.addDomListener(window, 'load', initialize);


// This function will iterate over markersData array
// creating markers with createMarker function
function displayMarkers(){

   // this variable sets the map bounds according to markers position
   var bounds = new google.maps.LatLngBounds();
   
   // for loop traverses markersData array calling createMarker function for each marker 
   for (var i = 0; i < markersData.length; i++){

      var latlng = new google.maps.LatLng(markersData[i].lat, markersData[i].lng);
      var name = markersData[i].name;
      var address1 = markersData[i].address1;
      var address2 = markersData[i].address2;
      var postalCode = markersData[i].postalCode;

      createMarker(latlng, name, address1, address2, postalCode);

      // marker position is added to bounds variable
      bounds.extend(latlng);  
   }

   // Finally the bounds variable is used to set the map bounds
   // with fitBounds() function
   map.fitBounds(bounds);
}

// This function creates each marker and it sets their Info Window content
function createMarker(latlng, name, address1, address2, postalCode){
   var marker = new google.maps.Marker({
      map: map,
      position: latlng,
      title: name
   });

   // This event expects a click on a marker
   // When this event is fired the Info Window content is created
   // and the Info Window is opened.
   google.maps.event.addListener(marker, 'click', function() {
      
      // Creating the content to be inserted in the infowindow
      var iwContent = '<div id="iw_container">' +
            '<div class="iw_title">' + name + '</div>' +
         '<div class="iw_content">' + address1 + '<br />' +
         address2 + '<br />' +
         postalCode + '</div></div>';
      
      // including content to the Info Window.
      infoWindow.setContent(iwContent);

      // opening the Info Window in the current map and at the current marker location.
      infoWindow.open(map, marker);
   });
}

</script>