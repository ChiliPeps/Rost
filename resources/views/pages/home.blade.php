@extends('master')

@section('content')
	@include('content.home.navigation')
	@include('content.home.carousel')
	@include('content.home.portfolio')
	@include('content.home.about')
	@include('content.home.contact')
	@include('content.home.localizacion')
	@include('content.home.portfoliomodal')
	
	
@stop