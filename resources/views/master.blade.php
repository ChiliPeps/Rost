<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href="css/stilo.css" rel="stylesheet" type="text/css">
	<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
	<!--
	<meta name="Keywords" content="radium, estación de radio, la paz, noticias y espectáculos de La Paz, Baja California Sur"/>
 	<meta name="Description" content="#YoSoyRadium Estación de radio con diferentes programas y con la información más importante de La Paz, Los Cabos, Comondú, Loreto y Mulegé." /> -->
 	
	{!! Html::favicon('favicon.png') !!}
	@include('inc/header_common')
	<title>Rosticeria California</title>

</head>
<body>
	@include('inc/header')

	<!-- Content -->
	@yield('content')
	<!-- Content -->

	@include('inc/footer')
	@include('inc/footer_common')

	<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
</body>
</html>