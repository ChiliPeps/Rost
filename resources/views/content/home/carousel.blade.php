<div class="kb-carousel">

	<div id="kb" class="carousel kb_elastic animate_text kb_wrapper" data-ride="carousel" data-interval="6000" data-pause="hover">

	    <!--======= Wrapper for Slides =======-->
	    <div class="carousel-inner" role="listbox">

	        <div class="item active">
	        	{!! Html::image("img/1.jpg", 'slider 1', ['class' => '']) !!}
	            <div class="carousel-caption kb_caption">
	                <h1 data-animation="animated flipInX"></h1>
	                <h2 data-animation="animated flipInX"></h2>
	            </div>
	        </div>

	        <div class="item">
	            {!! Html::image("img/2.jpg", 'slider 2', ['class' => '']) !!}
	            <div class="carousel-caption kb_caption kb_caption_right">
	                <h1 data-animation="animated flipInX"></h1>
	                <h2 data-animation="animated flipInX"></h2>
	            </div>
	        </div>

	        <div class="item">
	            {!! Html::image("img/3.jpg", 'slider 3', ['class' => '']) !!}
	            <div class="carousel-caption kb_caption kb_caption_center">
	                <h1 data-animation="animated flipInX"></h1>
	                <h2 data-animation="animated flipInX"></h2>
	            </div>
	        </div>

	    </div>

	    <!--======= Navigation Buttons =========-->
	    <!--======= Left Button =========-->
	    <a class="left carousel-control kb_control_left" href="#kb" role="button" data-slide="prev">
	        <span class="fa fa-angle-left kb_icons" aria-hidden="true"></span>
	        <span class="sr-only">Previous</span>
	    </a>
	    <!--======= Right Button =========-->
	    <a class="right carousel-control kb_control_right" href="#kb" role="button" data-slide="next">
	        <span class="fa fa-angle-right kb_icons" aria-hidden="true"></span>
	        <span class="sr-only">Next</span>
	    </a>

	</div> 
	
</div>